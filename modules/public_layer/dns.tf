#==============================================================
# Public / dns.tf
#==============================================================

#--------------------------------------------------------------
# DNS Zone
#--------------------------------------------------------------

# Before we create a dns entry we need to find a zone to put it
# in

data "aws_route53_zone" "zone" {
  name         = "${var.dns_zone}."
  private_zone = false
}

# Create the dns entry using the route53 zone

resource "aws_route53_record" "www" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"

  # get the dns name ie: dev.vocabs.ga.gov.au
  name = "${lookup(var.dns_name, var.environment)}${var.dns_zone}"
  type = "A"

  alias {
    name                   = "${aws_elb.elb.dns_name}"
    zone_id                = "${aws_elb.elb.zone_id}"
    evaluate_target_health = false
  }
}

#--------------------------------------------------------------
# SES Domain Identity 
#--------------------------------------------------------------


# We need to configure ses to be able to use our domain


# resource "aws_ses_domain_identity" "ses" {
#   domain = "${var.dns_zone}"
# }


# resource "aws_route53_record" "amazonses_verification_record" {
#   zone_id = "${data.aws_route53_zone.zone.zone_id}"
#   name    = "_amazonses.${var.dns_zone}"
#   type    = "TXT"
#   ttl     = "600"
#   records = ["${aws_ses_domain_identity.ses.verification_token}"]
# }

