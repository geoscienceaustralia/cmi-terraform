#==============================================================
# App / asg.tf
#==============================================================

# Autoscaling group to host our application server.

resource "aws_autoscaling_group" "asg" {
  lifecycle {
    create_before_destroy = true
  }

  vpc_zone_identifier = ["${aws_subnet.private.*.id}"]

  # Put the current LC name into the ASG to force an update
  name = "${var.stack_name}_${var.environment}_asg_${aws_launch_configuration.lc.name}"

  # Don't kill the old ASG until we have min capacity
  wait_for_elb_capacity     = "${var.asg_min}"
  wait_for_capacity_timeout = "10m"

  min_size             = "${var.asg_min}"
  max_size             = "${var.asg_max}"
  launch_configuration = "${aws_launch_configuration.lc.name}"
  load_balancers       = ["${var.elb_name}"]

  tag {
    key                 = "Name"
    value               = "${var.stack_name}_${var.environment}_asg"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "owner"
    value               = "${var.owner}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "environment"
    value               = "${var.environment}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "stack_name"
    value               = "${var.stack_name}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "created_by"
    value               = "terraform"
    propagate_at_launch = "true"
  }
}
