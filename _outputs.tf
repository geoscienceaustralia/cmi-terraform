#==============================================================
# outputs.tf
#==============================================================

# This file defines the information we want to write to the
# console after a succesful `terraform apply`

output "elb_dns_name" {
  description = "The url of the website"
  value       = "${module.public.dns_fqdn}"
}

output "jumpbox_ip" {
  description = "The IP address of the jumpbox"
  value       = "${module.public.jumpbox_ip}"
}

output "database_fqdn" {
  description = "The dns name of the database (internal only)"
  value       = "${module.dns.fqdn}"
}
