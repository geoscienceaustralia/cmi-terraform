# Overview

![Architecture Diagram]
(./img/architecture.png)

This will create:

 * An autoscaling web server group
 * An elastic file system
 * An elastic load-balancer
 * A Postgres database
 * A VPC with a NAT gateway and a jump host

## Preperation

1. Install Pre-requisites
    1. Download [AWSCLI](https://aws.amazon.com/cli/)
    2. Download [Terraform](https://www.terraform.io/)
    3. Download [Chamber](https://github.com/segmentio/chamber/releases)

2. Create a service user to run the Terraform Scripts
    1. Sign into the AWS Console
    2. Click Services
    3. Under `Security, Identity and Compliance` Select IAM
    4. Select Users
    5. Click Add user
    6. Give the account a username in the format: svcTF<appname>
    7. Select Programmatic access
    8. Click Next:Permissions
    9. Select: `Attach existing Policies Directly`
    10. Click Create Policy (it will open in a new tab)
    11. Next to Create Your Own Policy, Click `Select`
    12. Set the name to be `<appname>TerraformRunner`
    13. Copy the policy from the policies folder and paste it in the `Policy Document` field
    14. Click Create Policy
    15. Change tabs back to the User creation Tab
    16. In the AWS Console click Refresh (not the browser refresh button)
    17. Search for your new policy by it's name
    18. Click the checkbox on the left of the policy
    19. Click Next:Review (down the bottom)
    20. Click create user
    21. Copy the `access key id` and `secret access key`
    22. For a dev machine run `aws configure`
    23. For a bitbucket pipeline set the environment variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`

3. Create a new s3 bucket (this is used to store the current state of your infrastructure):
    1. Login to the AWS Console
    2. Click Services in the top Right
    3. Select S3 Under Storage
    4. Click Create Bucket
    5. Set the Bucket Name ( We usually use <accountname>-tfstate but it's up to your discretion)
    6. Select Sydney from the region dropdown
    7. Click Create

4. Configure Terraform to store it's state in the bucket you just created
    
```
terraform {
    required_version = ">= 0.9.1"
    backend "s3" {
        bucket = "INSERT_YOUR_BUCKET_NAME_HERE"
        key = "three-tier-dev/"
        region = "ap-southeast-2"
        lock_table = "terraform"
    }
}
```

5. Create a DynamoDB table in AWS:
    1. Login to the AWS Console
    2. Click Services in the top Right
    3. Under Databases, select DynamoDB
    4. Click Create Table
    5. Set Table Name to be terraform
    6. Set Primary Key to be LockID
    7. Leave all other options and select Create

Now if you run terraform init you will see :

```
Initializing the backend...

Backend configuration changed!
```
6. Create an ec2 keypair so you can access your instances.

## Set Variables

Set the following variables in variables.tf:

 *Terraform Remote State*
 * bucket - an s3 bucket that exists in your aws space (see Preperation)
 * lock_table - a dynamodb table in your aws space with the primary key LockID (see Preperation)

 *SSH Keys*
 * key_name - the ssh key to be added to your EC2 instances
 
Set the following environment variables (substituting the details of your application)
 
 * `export TF_VAR_db_admin_username=<adminusername>`
 * `export TF_VAR_db_admin_password=<adminpassword>`
 * `export TF_VAR_db_user_username=<applicationname>`
 * `export TF_VAR_db_user_password=<userpassword>`
 * `export TF_VAR_db_admin_password=<differentpass>`

For Example:
 * `export TF_VAR_db_admin_username=dbadmin`

## Creating your infrastructure

1. `terraform init`
2. `terraform get`
3. `terraform plan`
4. `terraform apply`

This command will output your database endpoint, which you will need below.

## Destroying your infrastructure

1. `terraform destroy`

This is assuming that you ran `terraform init` previously on the same machine
This command will tear down everything that terraform created.


## Connecting to database

From one of the application servers, you can connect to the RDS by running:

`mysql -h <endpointname> -u <username> -p`

You will be prompted to enter your password
you can then run: `use <database>` to allow you to query the database.

For example:
`mysql -h database.three-tier -u dbadmin -p`

`use application`