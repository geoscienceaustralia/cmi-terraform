#==============================================================
# App / iam_instance_profile.tf
#==============================================================

# Create an instance profile to allow our server to upload
# backups to an s3 bucket

# The Profile we attach to the launch config
resource "aws_iam_instance_profile" "efs_backup" {
  name = "${var.stack_name}_${var.environment}_instance_profile"
  role = "${aws_iam_role.efs_backup_role.id}"
}

# The policy to allow access to the bucket
resource "aws_iam_role_policy" "efs_backup_policy" {
  name = "${var.stack_name}_${var.environment}_s3_policy"
  role = "${aws_iam_role.efs_backup_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject*",
        "s3:Get*"
      ],
      "Resource": ["${aws_s3_bucket.efs_backup.arn}/*"]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:List*"
      ],
      "Resource": ["${aws_s3_bucket.efs_backup.arn}"]
    }
  ]
}
EOF
}

data "aws_kms_alias" "param_store_key" {
  name = "alias/${var.param_store_key}"
}

# The policy to allow access to the bucket
resource "aws_iam_role_policy" "ssm_policy" {
  name = "${var.stack_name}_${var.environment}_ssm_policy"
  role = "${aws_iam_role.efs_backup_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ssm:GetParameters"
      ],
      "Resource": ["arn:aws:ssm:ap-southeast-2:${data.aws_caller_identity.current.account_id}:parameter/${var.stack_name}-${var.environment}*"]
    },
    {
      "Effect": "Allow",
      "Action": [
        "ssm:DescribeParameters"
      ],
      "Resource": ["arn:aws:ssm:ap-southeast-2:${data.aws_caller_identity.current.account_id}:*"]
    },
    {
      "Effect": "Allow",
      "Action": [
          "kms:Decrypt",
          "kms:DescribeKey"
      ],
      "Resource": "arn:aws:kms:ap-southeast-2:${data.aws_caller_identity.current.account_id}:key/${data.aws_kms_alias.param_store_key.target_key_id}"
    }
  ]
}
EOF
}

# The policy to allow access to the bucket
resource "aws_iam_role_policy" "ses_policy" {
  name = "${var.stack_name}_${var.environment}_ses_policy"
  role = "${aws_iam_role.efs_backup_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "ses:ListIdentities",
          "ses:SendEmail"
      ],
      "Resource": [
          "*"
      ]
    }
  ]
}
EOF
}

# The Role itself
resource "aws_iam_role" "efs_backup_role" {
  name = "${var.stack_name}_${var.environment}efs_backup_role"
  path = "/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
