#==============================================================
# DNS / r53.tf
#==============================================================

# Create an Alias DNS record

resource "aws_route53_record" "alias_route" {
  zone_id = "${aws_route53_zone.zone.zone_id}"
  name    = "${var.dns_name}"
  type    = "A"

  alias {
    name                   = "${var.target}"
    zone_id                = "${var.target_hosted_zone_id}"
    evaluate_target_health = true
  }
}
