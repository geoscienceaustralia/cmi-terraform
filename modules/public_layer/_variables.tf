#==============================================================
# Public / outputs.tf
#==============================================================

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

variable "region" {}

variable "nat_gw_count" {
  default     = "3"
  description = "Number of NAT gateway instances"
}

variable "availability_zones" {
  type = "map"
}

variable "public_subnet_cidr" {
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  description = "List of public subnets"
}

variable "vpc_id" {}

variable "igw_id" {}

#--------------------------------------------------------------
# Tags
#--------------------------------------------------------------

variable "stack_name" {}

variable "environment" {}

variable "owner" {}

#--------------------------------------------------------------
# Jumpbox
#--------------------------------------------------------------

variable "jump_amis" {
  default = {
    "ap-southeast-2" = "ami-4d3b062e"
  }

  description = "AMI to be used for jumpbox"
}

variable "jump_instance_type" {
  default     = "t2.nano"
  description = "Instance type to be used for jumpbox"
}

variable "key_name" {}

variable "enable_jumpbox" {
  default     = false
  description = "Enable if ssh jumpbox is required"
}

#--------------------------------------------------------------
# Elastic Load Balancer
#--------------------------------------------------------------

variable "elb_check_path" {
  default = "/"
}

variable "listeners" {
  type = "map"
}

variable "port_num" {
  default = "80"
}

#--------------------------------------------------------------
# DNS Names
#--------------------------------------------------------------

variable "dns_zone" {}

variable "dns_name" {
  type = "map"
}
