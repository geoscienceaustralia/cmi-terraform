#==============================================================
# Public / outputs.tf
#==============================================================

#--------------------------------------------------------------
# Jumpbox
#--------------------------------------------------------------

output "jumpbox_ip" {
  value = "${aws_eip.jump.public_ip}"
}

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

output "public_subnet_ids" {
  value = ["${aws_subnet.public.*.id}"]
}

output "ngw_ids" {
  value = ["${aws_nat_gateway.nat.*.id}"]
}

#--------------------------------------------------------------
# Elastic Load Balancer
#--------------------------------------------------------------

output "elb_http_inbound_sg_id" {
  value = "${aws_security_group.elb_http_inbound_sg.id}"
}

output "elb_https_inbound_sg_id" {
  value = "${aws_security_group.elb_https_inbound_sg.id}"
}

output "elb_outbound_sg_id" {
  value = "${aws_security_group.elb_outbound_sg.id}"
}

output "jump_ssh_sg_id" {
  value = "${aws_security_group.jump_ssh_sg.id}"
}

output "elb_name" {
  value = "${aws_elb.elb.name}"
}

output "elb_dns_name" {
  value = "${aws_elb.elb.dns_name}"
}

output "elb_dns_hosted_zone" {
  value = "${aws_elb.elb.zone_id}"
}

#--------------------------------------------------------------
# DNS
#--------------------------------------------------------------

output "dns_fqdn" {
  value = "${aws_route53_record.www.fqdn}"
}
