#==============================================================
# main.tf
#==============================================================

# This file is used to configure global settings and create our
# stack by calling individual modules.
# This file is not executed in order, if a module relies on
# the outputs of another terraform will build it in that order.

#--------------------------------------------------------------
# Global Config
#--------------------------------------------------------------

# Configure the cloud provider and define the terraform backend

provider "aws" {
  region = "${var.region}"
}

#--------------------------------------------------------------
# Shared Components
#--------------------------------------------------------------

# Create the Virtual Private Cloud and other global components.

module "shared" {
  source             = "modules/shared"
  stack_name         = "${var.stack_name}"
  environment        = "${var.environment}"
  owner              = "${var.owner}"
  availability_zones = "${var.availability_zones}"
}

#--------------------------------------------------------------
# Public Layer
#--------------------------------------------------------------

# Create a public subnetwork with a jumpbox and elastic load
# balancer. Create security groups for the jumpbox and ELB. 

module "public" {
  source             = "modules/public_layer"
  region             = "${var.region}"
  availability_zones = "${var.availability_zones}"
  stack_name         = "${var.stack_name}"
  environment        = "${var.environment}"
  owner              = "${var.owner}"
  key_name           = "${var.key_name}"
  enable_jumpbox     = true
  nat_gw_count       = "1"
  vpc_id             = "${module.shared.vpc_id}"
  igw_id             = "${module.shared.igw_id}"

  # dns
  dns_zone = "${var.dns_zone}"
  dns_name = "${var.dns_name}"

  listeners {
    "instance_port"     = "80"
    "instance_protocol" = "HTTP"
    "lb_port"           = "80"
    "lb_protocol"       = "HTTP"
  }
}

#--------------------------------------------------------------
# Application Layer
#--------------------------------------------------------------

# Create a private subnetwork with an autoscaling application
# server and a Elastic File System. Insert secrets into
# the userdata script so that the application mounts our EFS 
# and installs app.

module "app" {
  source             = "modules/app_layer"
  availability_zones = "${var.availability_zones}"
  stack_name         = "${var.stack_name}"
  environment        = "${var.environment}"
  owner              = "${var.owner}"
  key_name           = "${var.key_name}"

  # Security groups
  elb_http_inbound_sg_id  = "${module.public.elb_http_inbound_sg_id}"
  elb_https_inbound_sg_id = "${module.public.elb_https_inbound_sg_id}"
  jump_ssh_sg_id          = "${module.public.jump_ssh_sg_id}"

  elb_name = "${module.public.elb_name}"
  ngw_ids  = "${module.public.ngw_ids}"
  vpc_id   = "${module.shared.vpc_id}"
  asg_amis = "${data.aws_ami.app_ami.image_id}"

  # Userdata script variables
  rds_endpoint = "${module.dns.fqdn}"
  rds_port     = "${var.db_port}"
}

#--------------------------------------------------------------
# Database Layer
#--------------------------------------------------------------

# Create a database subnetwork an RDS Subnetwork Group and a 
# Relational Database

module "database" {
  source             = "modules/database_layer"
  availability_zones = "${var.availability_zones}"
  stack_name         = "${var.stack_name}"
  environment        = "${var.environment}"
  owner              = "${var.owner}"
  vpc_id             = "${module.shared.vpc_id}"
  db_name            = "${var.db_name}"
  rds_is_multi_az    = "${var.rds_is_multi_az}"
  username           = "${data.aws_ssm_parameter.db_admin_username.value}"
  password           = "${data.aws_ssm_parameter.db_admin_password.value}"
  db_port_num        = "${var.db_port}"
  app_sg_id          = "${module.app.app_sg_id}"
}

#--------------------------------------------------------------
# Database DNS Endpoint
#--------------------------------------------------------------

# Create a dns zone and entry to allow the Application Server
# to talk to the RDS

module "dns" {
  source                = "modules/dns"
  zone                  = "${var.stack_name}"
  stack_name            = "${var.stack_name}"
  environment           = "${var.environment}"
  owner                 = "${var.owner}"
  vpc_id                = "${module.shared.vpc_id}"
  dns_name              = "database"
  target_hosted_zone_id = "${module.database.database_hosted_zone_id}"
  target                = "${module.database.database_address}"
}
