#==============================================================
# variables.tf
#==============================================================

# This file is used to set variables that are passed to sub 
# modules to build our stack

#--------------------------------------------------------------
# Terraform Remote State
#--------------------------------------------------------------

# Define the remote objects that terraform will use to store
# state. We use a remote store, so that you can run destroy
# from a seperate machine to the one it was built on.

terraform {
  required_version = ">= 0.9.1"

  backend "s3" {
    # This is an s3bucket you will need to create in your aws 
    # space
    bucket = "tfstate-262151965814"

    # The key should be unique to each stack, because we want to
    # have multiple enviornments alongside each other we set
    # this dynamically in the bitbucket-pipelines.yml with the
    # --backend
    key = "cmi-dev/"

    region = "ap-southeast-2"

    # This is a DynamoDB table with the Primary Key set to LockID
    dynamodb_table = "terraform-lock"
  }
}

#--------------------------------------------------------------
# Global Config
#--------------------------------------------------------------

# Variables used in the global config

variable "region" {
  description = "The AWS region we want to build this stack in"
  default     = "ap-southeast-2"
}

variable "stack_name" {
  description = "The name of our application"
  default     = "cmi"
}

variable "owner" {
  description = "A group email address to be used in tags"
  default     = "autobots@ga.gov.au"
}

variable "environment" {
  description = "Used for seperating terraform backends and naming items"
  default     = "dev"
}

variable "availability_zones" {
  description = "Geographically distanced areas inside the region"

  default = {
    "0" = "ap-southeast-2a"
    "1" = "ap-southeast-2b"
    "2" = "ap-southeast-2c"
  }
}

variable "key_name" {
  description = "AWS EC2 Keypair to be configured on the servers"
  default     = "cmi-test-key"
}

#--------------------------------------------------------------
# Database
#--------------------------------------------------------------

# Variables used in the database config

variable "db_name" {
  description = "The name of our rds"
  default     = "CMI"
}

variable "rds_is_multi_az" {
  description = "Create backup database in seperate availability zone"
  default     = "false"
}

variable "db_port" {
  description = "The port the Application Server will access the database on"
  default     = 3306
}

#--------------------------------------------------------------
# Userdata
#--------------------------------------------------------------

# Variables used in the userdata.sh script
# These are set in bitbucket and have no default value.
# Environment variables with the format TF_VAR_var_name will be
# used by terraform as the value of these variables.

data "aws_ssm_parameter" "db_admin_username" {
  name = "${var.stack_name}-${var.environment}.mysql_root_username"
}

data "aws_ssm_parameter" "db_admin_password" {
  name = "${var.stack_name}-${var.environment}.mysql_root_pwd"
}

variable "efs_mount_point" {
  description = "The folder to mount the EFS on"
  default     = "/var/www/html/sites/default/files/"
}

#--------------------------------------------------------------
# DNS Settings
#--------------------------------------------------------------

# Define our DNS settings

variable "dns_zone" {
  description = "The manually created route53 zone we want to use"
  default     = "cmi.ga.gov.au"
}

variable "dns_name" {
  description = "The mapping for stack names to dns entries"

  default = {
    # dev.cmi.ga.gov.au
    "dev" = "dev."

    # test.cmi.ga.gov.au
    "test" = "test."

    # cmi.ga.gov.au
    "prod" = ""
  }
}

#--------------------------------------------------------------
# Server Images
#--------------------------------------------------------------

# Search for our server images

data "aws_ami" "app_ami" {
  most_recent = true
  owners      = ["self"]

  filter {
    name = "tag:application"

    # Change this if you are using a different image
    values = ["CMI Drupal"]
  }

  filter {
    name   = "tag:version"
    values = ["${var.environment}"]
  }
}
