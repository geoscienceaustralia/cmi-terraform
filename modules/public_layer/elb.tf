#==============================================================
# Public / elb.tf
#==============================================================

# Create an Elastic Load Balancer to route traffic to our
# Autoscaling group, and health check the app server.

resource "aws_elb" "elb" {
  name    = "${var.stack_name}-${var.environment}-elb"
  subnets = ["${aws_subnet.public.*.id}"]

  listener {
    instance_port     = "${lookup(var.listeners,"instance_port","80")}"
    instance_protocol = "${lookup(var.listeners,"instance_protocol","HTTP")}"
    lb_port           = "${lookup(var.listeners,"lb_port","80")}"
    lb_protocol       = "${lookup(var.listeners,"lb_protocol","HTTP")}"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 30

    target = "HTTP:8080/healthz"

    interval = 60
  }

  security_groups = [
    "${aws_security_group.elb_http_inbound_sg.id}",
    "${aws_security_group.elb_https_inbound_sg.id}",
  ]

  tags {
    Name       = "${var.stack_name}_${var.environment}_elb"
    owner      = "${var.owner}"
    stack_name = "${var.stack_name}"
    created_by = "terraform"
  }
}
