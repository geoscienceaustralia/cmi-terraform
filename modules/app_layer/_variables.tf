#==============================================================
# App / variables.tf
#==============================================================

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

variable "private_subnet_cidr" {
  default     = ["10.0.51.0/24", "10.0.52.0/24", "10.0.53.0/24"]
  description = "List of private subnets"
}

variable "availability_zones" {
  type = "map"
}

variable "vpc_id" {}

variable "port_num" {
  default = "80"
}

#--------------------------------------------------------------
# Application Server
#--------------------------------------------------------------

variable "asg_amis" {
  default     = "ami-26575d45"
  description = "AMI to be used in the ASG"
}

variable "instance_type" {
  default     = "t2.micro"
  description = "Default instance type"
}

variable "elb_http_inbound_sg_id" {}

variable "elb_https_inbound_sg_id" {}

variable "jump_ssh_sg_id" {}

variable "userdata_filepath" {
  default = "./userdata.tpl"
}

variable "key_name" {}

#--------------------------------------------------------------
# Userdata Script
#--------------------------------------------------------------

variable "efs_mount_point" {
  description = "The folder to mount the EFS on"
  default     = "/var/www/html/sites/default/files/"
}

variable "rds_port" {}

variable "rds_endpoint" {}

#--------------------------------------------------------------
# Autoscaling Group
#--------------------------------------------------------------

variable "asg_min" {
  default     = "1"
  description = "Minimum number of instances"
}

variable "asg_max" {
  default     = "1"
  description = "Maximum number of instances"
}

variable "elb_name" {}

#--------------------------------------------------------------
# Tags
#--------------------------------------------------------------

variable "stack_name" {}

variable "environment" {}

variable "owner" {}

variable "ngw_ids" {
  type = "list"
}

data "aws_caller_identity" "current" {}

variable "param_store_key" {
  default = "parameter_store_key"
}
