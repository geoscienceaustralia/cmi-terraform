#==============================================================
# Shared / variables.tf
#==============================================================

variable "availability_zones" {
  description = "The AZs to build the VPC in"
  type        = "map"
}

variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
  default     = "10.0.0.0/16"
}

variable "environment" {
  description = "Environment - dev, staging, prod, etc."
  default     = "dev"
}

variable "stack_name" {
  description = "The name of this application"
}

variable "owner" {
  description = "A group email to be tagged on all objects"
}
