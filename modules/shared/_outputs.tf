#==============================================================
# Shared / outputs.tf
#==============================================================

output "vpc_id" {
  description = "The ID of the Virtual Private Cloud"
  value       = "${aws_vpc.vpc.id}"
}

output "igw_id" {
  description = "The ID of the Internet Gateway"
  value       = "${aws_internet_gateway.default.id}"
}
