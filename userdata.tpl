#!/bin/bash
exec > /var/log/user-data.log 2>&1

#==============================================================
# userdata.tpl
#==============================================================

# This is a template that is used to generate the userdata.sh
# that will be run on the application server when it is built.
# The variables are replaced during a terraform apply.

#--------------------------------------------------------------
# Elastic File System
#--------------------------------------------------------------

# Mount the EFS
sudo mkdir ${efs_mount_point}
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 ${efs_id}.efs.ap-southeast-2.amazonaws.com:/ ${efs_mount_point}

#--------------------------------------------------------------
# Install App
#--------------------------------------------------------------

# Insert dynamic vars we don't need to store in param store
export RDS_ENDPOINT=${rds_endpoint}
export RDS_PORT=${rds_port}

# Secrets stored in SSM Paramater store are accessed via chamber
chamber exec ${stack_name}-${environment} -- /opt/install.sh

#--------------------------------------------------------------
# Create backup cronjob
#--------------------------------------------------------------

# Insert bucket name into backup script.php
sudo sed -i "s/EFS_BACKUP_BUCKET/${efs_backup_bucket}/" /usr/local/bin/efs-backup.sh

# create cron job
crontab -l >mycron
# 1 am AEST
echo -e "00 15 * * * /usr/local/bin/efs-backup.sh\n" >> mycron
crontab mycron
rm mycron


#--------------------------------------------------------------
# Start Healthcheck
#--------------------------------------------------------------

# We use goss for healthchecking
# This will create an endpoint at :8080/healthz that responds
# With HTTP 200 if the checks pass or 500 if they fail

cd /opt/healthz
goss serve &