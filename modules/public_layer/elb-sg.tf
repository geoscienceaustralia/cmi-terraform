#==============================================================
# Public / elb-sg.tf
#==============================================================

# Security groups for Elastic Load Balancer

resource "aws_security_group" "elb_http_inbound_sg" {
  # Allow HTTP from anywhere
  name = "elb_http_inbound"

  ingress {
    from_port   = "${var.port_num}"
    to_port     = "${var.port_num}"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags {
    Name        = "${var.stack_name}_${var.environment}_elb_http_inbound"
    owner       = "${var.owner}"
    stack_name  = "${var.stack_name}"
    environment = "${var.environment}"
    created_by  = "terraform"
  }
}

resource "aws_security_group" "elb_https_inbound_sg" {
  # Allow HTTPS from anywhere
  name = "elb_https_inbound"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags {
    Name        = "${var.stack_name}_${var.environment}_elb_https_inbound"
    owner       = "${var.owner}"
    stack_name  = "${var.stack_name}"
    environment = "${var.environment}"
    created_by  = "terraform"
  }
}

resource "aws_security_group" "elb_outbound_sg" {
  # Allow outbound connections
  name = "elb_outbound"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags {
    Name        = "${var.stack_name}_${var.environment}_elb_outbound"
    owner       = "${var.owner}"
    stack_name  = "${var.stack_name}"
    environment = "${var.environment}"
    created_by  = "terraform"
  }
}
