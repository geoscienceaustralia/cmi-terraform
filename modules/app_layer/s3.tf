#==============================================================
# App / s3.tf
#==============================================================

# Create a bucket used to store backups

resource "aws_s3_bucket" "efs_backup" {
  bucket = "efs-backup-${var.stack_name}-${var.environment}-${data.aws_caller_identity.current.account_id}"
  acl    = "private"

  tags {
    owner = "${var.owner}"
  }
}
