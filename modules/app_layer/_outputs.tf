#==============================================================
# App / outputs.tf
#==============================================================

output "asg_id" {
  value = "${aws_autoscaling_group.asg.id}"
}

output "webapp_lc_id" {
  value = "${aws_launch_configuration.lc.id}"
}

output "webapp_lc_name" {
  value = "${aws_launch_configuration.lc.name}"
}

output "efs_id" {
  value = "${aws_efs_file_system.efs.id}"
}

output "app_sg_id" {
  value = "${aws_security_group.app_sg.id}"
}
